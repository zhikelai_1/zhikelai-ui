import request from '@/utils/request'

// 查询短信配置列表
export function listConfigInfo(query) {
  return request({
    url: 'zhike/sms/configInfo/list',
    method: 'get',
    params: query
  })
}

// 查询短信配置详细
export function getConfigInfo(id) {
  return request({
    url: '/zhike/sms/configInfo/' + id,
    method: 'get'
  })
}

// 新增短信配置
export function addConfigInfo(data) {
  return request({
    url: '/zhike/sms/configInfo',
    method: 'post',
    data: data
  })
}

// 修改短信配置
export function updateConfigInfo(data) {
  return request({
    url: '/zhike/sms/configInfo',
    method: 'put',
    data: data
  })
}

// 删除短信配置
export function delConfigInfo(id) {
  return request({
    url: '/zhike/sms/configInfo/' + id,
    method: 'delete'
  })
}
