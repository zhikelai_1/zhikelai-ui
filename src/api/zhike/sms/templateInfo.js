import request from '@/utils/request'

// 查询短信模板列表
export function listTemplateInfo(query) {
  return request({
    url: '/zhike/sms/templateInfo/list',
    method: 'get',
    params: query
  })
}

// 查询短信模板详细
export function getTemplateInfo(id) {
  return request({
    url: '/zhike/sms/templateInfo/' + id,
    method: 'get'
  })
}

// 新增短信模板
export function addTemplateInfo(data) {
  return request({
    url: '/zhike/sms/templateInfo',
    method: 'post',
    data: data
  })
}

// 修改短信模板
export function updateTemplateInfo(data) {
  return request({
    url: '/zhike/sms/templateInfo',
    method: 'put',
    data: data
  })
}

// 删除短信模板
export function delTemplateInfo(id) {
  return request({
    url: '/zhike/sms/templateInfo/' + id,
    method: 'delete'
  })
}
