import request from '@/utils/request'

// 查询短信发送记录列表
export function listRecordInfo(query) {
  return request({
    url: '/zhike/sms/recordInfo/list',
    method: 'get',
    params: query
  })
}

// 查询短信发送记录详细
export function getRecordInfo(id) {
  return request({
    url: '/zhike/sms/recordInfo/' + id,
    method: 'get'
  })
}

// 新增短信发送记录
export function addRecordInfo(data) {
  return request({
    url: '/zhike/sms/recordInfo',
    method: 'post',
    data: data
  })
}

// 修改短信发送记录
export function updateRecordInfo(data) {
  return request({
    url: '/zhike/sms/recordInfo',
    method: 'put',
    data: data
  })
}

// 删除短信发送记录
export function delRecordInfo(id) {
  return request({
    url: '/zhike/sms/recordInfo/' + id,
    method: 'delete'
  })
}
