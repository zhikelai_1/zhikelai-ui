import request from '@/utils/request'

// 查询商户详情信息列表
export function listDetailInfo(query) {
  return request({
    url: '/zhike/merchant/detailInfo/list',
    method: 'get',
    params: query
  })
}

// 查询商户详情信息详细
export function getDetailInfo(infoId) {
  return request({
    url: '/zhike/merchant/detailInfo/' + infoId,
    method: 'get'
  })
}

// 新增商户详情信息
export function addDetailInfo(data) {
  return request({
    url: '/zhike/merchant/detailInfo',
    method: 'post',
    data: data
  })
}

// 修改商户详情信息
export function updateDetailInfo(data) {
  return request({
    url: '/zhike/merchant/detailInfo',
    method: 'put',
    data: data
  })
}

// 删除商户详情信息
export function delDetailInfo(infoId) {
  return request({
    url: '/zhike/merchant/detailInfo/' + infoId,
    method: 'delete'
  })
}
