import request from '@/utils/request'

// 查询商户基本信息列表
export function listBaseInfo(query) {
  return request({
    url: '/zhike/merchant/baseInfo/list',
    method: 'get',
    params: query
  })
}

// 查询商户基本信息详细
export function getBaseInfo(id) {
  return request({
    url: '/zhike/merchant/baseInfo/' + id,
    method: 'get'
  })
}

// 新增商户基本信息
export function addBaseInfo(data) {
  return request({
    url: '/zhike/merchant/baseInfo',
    method: 'post',
    data: data
  })
}

// 修改商户基本信息
export function updateBaseInfo(data) {
  return request({
    url: '/zhike/merchant/baseInfo',
    method: 'put',
    data: data
  })
}

// 删除商户基本信息
export function delBaseInfo(id) {
  return request({
    url: '/zhike/merchant/baseInfo/' + id,
    method: 'delete'
  })
}
