import request from '@/utils/request'

// 查询商户套餐信息列表
export function listMealInfo(query) {
  return request({
    url: '/zhike/merchant/mealInfo/list',
    method: 'get',
    params: query
  })
}

// 查询商户套餐信息详细
export function getMealInfo(id) {
  return request({
    url: '/zhike/merchant/mealInfo/' + id,
    method: 'get'
  })
}

// 新增商户套餐信息
export function addMealInfo(data) {
  return request({
    url: '/zhike/merchant/mealInfo',
    method: 'post',
    data: data
  })
}

// 修改商户套餐信息
export function updateMealInfo(data) {
  return request({
    url: '/zhike/merchant/mealInfo',
    method: 'put',
    data: data
  })
}

// 删除商户套餐信息
export function delMealInfo(id) {
  return request({
    url: '/zhike/merchant/mealInfo/' + id,
    method: 'delete'
  })
}
