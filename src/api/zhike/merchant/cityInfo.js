import request from '@/utils/request'

// 查询商户服务城市信息列表
export function listCityInfo(query) {
  return request({
    url: '/zhike/merchant/cityInfo/list',
    method: 'get',
    params: query
  })
}

// 查询商户服务城市信息详细
export function getCityInfo(id) {
  return request({
    url: '/zhike/merchant/cityInfo/' + id,
    method: 'get'
  })
}

// 新增商户服务城市信息
export function addCityInfo(data) {
  return request({
    url: '/zhike/merchant/cityInfo',
    method: 'post',
    data: data
  })
}

// 修改商户服务城市信息
export function updateCityInfo(data) {
  return request({
    url: '/zhike/merchant/cityInfo',
    method: 'put',
    data: data
  })
}

// 删除商户服务城市信息
export function delCityInfo(id) {
  return request({
    url: '/zhike/merchant/cityInfo/' + id,
    method: 'delete'
  })
}
