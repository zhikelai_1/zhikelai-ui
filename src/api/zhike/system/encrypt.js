import request from '@/utils/request'

// 查询app加密配置列表
export function listEncrypt(query) {
  return request({
    url: '/zhike/system/encrypt/list',
    method: 'get',
    params: query
  })
}

// 查询app加密配置详细
export function getEncrypt(id) {
  return request({
    url: '/zhike/system/encrypt/' + id,
    method: 'get'
  })
}

// 新增app加密配置
export function addEncrypt(data) {
  return request({
    url: '/zhike/system/encrypt',
    method: 'post',
    data: data
  })
}

// 修改app加密配置
export function updateEncrypt(data) {
  return request({
    url: '/zhike/system/encrypt',
    method: 'put',
    data: data
  })
}

// 删除app加密配置
export function delEncrypt(id) {
  return request({
    url: '/zhike/system/encrypt/' + id,
    method: 'delete'
  })
}
