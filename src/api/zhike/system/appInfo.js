import request from '@/utils/request'

// 查询应用包管理列表
export function listAppInfo(query) {
  return request({
    url: '/zhike/system/appInfo/list',
    method: 'get',
    params: query
  })
}

// 查询应用包管理详细
export function getAppInfo(id) {
  return request({
    url: '/zhike/system/appInfo/' + id,
    method: 'get'
  })
}

// 新增应用包管理
export function addAppInfo(data) {
  return request({
    url: '/zhike/system/appInfo',
    method: 'post',
    data: data
  })
}

// 修改应用包管理
export function updateAppInfo(data) {
  return request({
    url: '/zhike/system/appInfo',
    method: 'put',
    data: data
  })
}

// 删除应用包管理
export function delAppInfo(id) {
  return request({
    url: '/zhike/system/appInfo/' + id,
    method: 'delete'
  })
}
