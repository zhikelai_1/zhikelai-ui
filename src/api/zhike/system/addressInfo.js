import request from '@/utils/request'

// 查询地址信息管理列表
export function listAddressInfo(query) {
  return request({
    url: '/zhike/system/addressInfo/list',
    method: 'get',
    params: query
  })
}

// 查询地址信息管理详细
export function getAddressInfo(cityCode) {
  return request({
    url: '/zhike/system/addressInfo/' + cityCode,
    method: 'get'
  })
}

// 新增地址信息管理
export function addAddressInfo(data) {
  return request({
    url: '/zhike/system/addressInfo',
    method: 'post',
    data: data
  })
}

// 修改地址信息管理
export function updateAddressInfo(data) {
  return request({
    url: '/zhike/system/addressInfo',
    method: 'put',
    data: data
  })
}

// 删除地址信息管理
export function delAddressInfo(cityCode) {
  return request({
    url: '/zhike/system/addressInfo/' + cityCode,
    method: 'delete'
  })
}
