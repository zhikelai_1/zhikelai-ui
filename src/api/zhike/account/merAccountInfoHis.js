import request from '@/utils/request'

// 查询商户账户流水列表
export function listMerAccountInfoHis(query) {
  return request({
    url: '/zhike/account/merAccountInfoHis/list',
    method: 'get',
    params: query
  })
}

// 查询商户账户流水详细
export function getMerAccountInfoHis(id) {
  return request({
    url: '/zhike/account/merAccountInfoHis/' + id,
    method: 'get'
  })
}

// 新增商户账户流水
export function addMerAccountInfoHis(data) {
  return request({
    url: '/zhike/account/merAccountInfoHis',
    method: 'post',
    data: data
  })
}

// 修改商户账户流水
export function updateMerAccountInfoHis(data) {
  return request({
    url: '/zhike/account/merAccountInfoHis',
    method: 'put',
    data: data
  })
}

// 删除商户账户流水
export function delMerAccountInfoHis(id) {
  return request({
    url: '/zhike/account/merAccountInfoHis/' + id,
    method: 'delete'
  })
}
