import request from '@/utils/request'

// 查询商户账户信息列表
export function listMerAccountInfo(query) {
  return request({
    url: '/zhike/account/merAccountInfo/list',
    method: 'get',
    params: query
  })
}

// 查询商户账户信息详细
export function getMerAccountInfo(accountId) {
  return request({
    url: '/zhike/account/merAccountInfo/' + accountId,
    method: 'get'
  })
}

// 新增商户账户信息
export function addMerAccountInfo(data) {
  return request({
    url: '/zhike/account/merAccountInfo',
    method: 'post',
    data: data
  })
}

// 修改商户账户信息
export function updateMerAccountInfo(data) {
  return request({
    url: '/zhike/account/merAccountInfo',
    method: 'put',
    data: data
  })
}

// 删除商户账户信息
export function delMerAccountInfo(accountId) {
  return request({
    url: '/zhike/account/merAccountInfo/' + accountId,
    method: 'delete'
  })
}
