import request from '@/utils/request'

// 查询总账户流水列表
export function listAccountInfoHis(query) {
  return request({
    url: '/zhike/account/accountInfoHis/list',
    method: 'get',
    params: query
  })
}

// 查询总账户流水详细
export function getAccountInfoHis(id) {
  return request({
    url: '/zhike/account/accountInfoHis/' + id,
    method: 'get'
  })
}

// 新增总账户流水
export function addAccountInfoHis(data) {
  return request({
    url: '/zhike/account/accountInfoHis',
    method: 'post',
    data: data
  })
}

// 修改总账户流水
export function updateAccountInfoHis(data) {
  return request({
    url: '/zhike/account/accountInfoHis',
    method: 'put',
    data: data
  })
}

// 删除总账户流水
export function delAccountInfoHis(id) {
  return request({
    url: '/zhike/account/accountInfoHis/' + id,
    method: 'delete'
  })
}
