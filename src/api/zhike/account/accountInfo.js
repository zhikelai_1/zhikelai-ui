import request from '@/utils/request'

// 查询总账户信息列表
export function listAccountInfo(query) {
  return request({
    url: '/zhike/account/accountInfo/list',
    method: 'get',
    params: query
  })
}

// 查询总账户信息详细
export function getAccountInfo(id) {
  return request({
    url: '/zhike/account/accountInfo/' + id,
    method: 'get'
  })
}

// 新增总账户信息
export function addAccountInfo(data) {
  return request({
    url: '/zhike/account/accountInfo',
    method: 'post',
    data: data
  })
}

// 修改总账户信息
export function updateAccountInfo(data) {
  return request({
    url: '/zhike/account/accountInfo',
    method: 'put',
    data: data
  })
}

// 删除总账户信息
export function delAccountInfo(id) {
  return request({
    url: '/zhike/account/accountInfo/' + id,
    method: 'delete'
  })
}
